class CreateAuthentications < ActiveRecord::Migration
  def up
    create_table :user_group_authentications do |t|
      t.integer :user_id
      t.string :provider
      t.string :uid
    end
  end

  def down
    drop_table :user_group_authentications
  end
end
